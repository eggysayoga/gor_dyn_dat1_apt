﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class smoothlyTransitionLayer : MonoBehaviour {

    //public
    public Animator currentAnimator;
    public bool triggerNew = false;

    // private
    private float weightoz, setweightoz;
    private bool goingUp = true;
    private bool weAreMoving = false;

	// Use this for initialization
	void Start () {
        weightoz = 0;
	}

    // Update is called once per frame
    void Update()
    {

        // we only start moving if something triggers us off
        if (triggerNew) 
        {
            weAreMoving = true;
            triggerNew = false;
        }

        if (weAreMoving)
        {
            // going down from 1 to 0
            if (!goingUp)
            {
                // move the float a little each frame
                weightoz = weightoz - Time.deltaTime / 1.5f;
                //Debug.Log("weightoz: " + weightoz);

                // now lets apply a smoothing to that
                setweightoz = Mathf.SmoothStep(1, 0, Mathf.SmoothStep(1, 0, weightoz));
                Debug.Log("going down  setweightoz: " + setweightoz);

                currentAnimator.SetLayerWeight(1, setweightoz);

                if (weightoz < 0)
                {
                    weightoz = 0;
                    weAreMoving = false;
                    goingUp = true;
                }
            }

            // going up from 0 to 1
            else
            {
                // move the float a little each frame
                weightoz = weightoz + Time.deltaTime / 1.5f;
                //Debug.Log("going up weightoz: " + weightoz);

                // now lets apply a smoothing to that
                setweightoz = Mathf.SmoothStep(0, 1, Mathf.SmoothStep(0, 1, weightoz));
                Debug.Log("going up setweightoz: " + setweightoz);

                currentAnimator.SetLayerWeight(1, setweightoz);

                if (weightoz > 1)
                {
                    weightoz = 1;
                    weAreMoving = false;
                    goingUp = false;
                }
            }



        }


      

    }

}
